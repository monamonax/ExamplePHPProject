<?php
/**
 * Yii.php file
 *
 * Date: 26.03.17
 * Time: 13:27
 * @filename Yii.php
 * @author   Max Goryavin <monamonax@mail.ru>
 *
 */

/**
 * Class Yii
 */
class Yii extends \yii\BaseYii
{
    /**
     * @inheritdoc
     */
    public static function createObject($type, array $params = [])
    {
        $className = null;
        if (is_string($type))
            $type = ['class' => $type];
        return parent::createObject($type, $params);
    }
}

spl_autoload_register(['Yii', 'autoload'], true, true);
Yii::$classMap  = require_once(YII2_PATH . '/classes.php');
Yii::$container = new \yii\di\Container();
