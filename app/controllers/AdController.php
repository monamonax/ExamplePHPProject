<?php
/**
 * AdController.php file
 *
 * Date: 29.03.17
 * Time: 2:14
 * @filename AdController.php
 * @author   Max Goryavin <monamonax@mail.ru>
 */

namespace app\controllers;


use app\assets\AppAsset;
use app\models\Ad;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Class AdController
 * @package  app\controllers
 * @author   Max Goryavin <monamonax@mail.ru>
 */
class AdController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @inheritDoc
     */
    public function beforeAction($action)
    {
        $beforeAction = parent::beforeAction($action);
        if ($beforeAction && !\Yii::$app->request->isAjax) {
            AppAsset::register(\Yii::$app->view);
        }
        return $beforeAction;
    }


    /**
     * Lists all Ad models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel       = new Ad();
        $dataProvider      = $searchModel->search(\Yii::$app->request->queryParams);
        $this->view->title = '';
        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Ad model.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Ad model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Ad();
        $model->setScenario('create');

        $this->view->title = \Yii::t('app', 'Добавление нового объявления');
        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Finds the Ad model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return Ad the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ad::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}