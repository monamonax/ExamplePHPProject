<?php
/**
 * HomeController.php file
 *
 * Date: 26.03.17
 * Time: 13:51
 * @filename HomeController.php
 * @author   Max Goryavin <monamonax@mail.ru>
 *
 */

namespace app\controllers;


use yii\web\Controller;

/**
 * Class HomeController
 * @package  app\controllers
 * @author   Max Goryavin <monamonax@mail.ru>
 *
 */
class HomeController extends Controller
{

    /**
     * @return string
     */
    public function actionIndex()
    {
        return 'Test App';
    }
}