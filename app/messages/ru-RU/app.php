<?php
/**
 * app.php file
 *
 * Date: 29.03.17
 * Time: 2:33
 * @filename app.php
 * @author   Max Goryavin <monamonax@mail.ru>
 */
return [
    'address'     => 'Адрес',
    'name'        => 'Имя собственника',
    'phone'       => 'Телефон собственника',
    'status'      => 'Статус',
    'type'        => 'Продажа/аренда',
    'price'       => 'Цена',
    'owner'       => 'Собственник/Риэлтор',
    'floor'       => 'Этаж',
    'storeys'     => 'Этажность',
    'rooms'       => 'Число комнат',
    'area'        => 'Общая площадь',
    'description' => 'Описание',
    'source'      => 'Источник',

];