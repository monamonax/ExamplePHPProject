<?php
/**
 * ModerationAd.php file
 *
 * Date: 29.03.17
 * Time: 4:00
 * @filename ModerationAd.php
 * @author   Max Goryavin <monamonax@mail.ru>
 */

namespace app\jobs;


use app\models\Ad;
use yii\base\Object;
use yii\db\ActiveQuery;
use zhuravljov\yii\queue\Job;

/**
 * Class ModerationAd
 * @package  app\jobs
 * @author   Max Goryavin <monamonax@mail.ru>
 */
class ModerationAd extends Object implements Job
{
    /**
     * @var integer
     */
    public $primaryKey;

    /**
     * Собственно запускаем модерацию
     */
    public function run()
    {
        $pk = $this->primaryKey;
        if ($pk) {
            /** @var ActiveQuery $query */
            $query = Ad::find();
            $query->where(['id' => $pk]);
            /** @var Ad $model */
            $model = $query->one();
            if ($model) {
                $model->automaticModeration();
            }
        }
    }
}