<?php
/**
 * LTIE9Asset.php file
 *
 * Date: 16.02.17
 * Time: 2:08
 * @filename LTIE9Asset.php
 * @author   Max Goryavin <monamonax@mail.ru>
 */

namespace app\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Class LTIE9Asset
 * @package app\assets
 */
class LTIE9Asset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public $js = [
        'https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js',
        'https://oss.maxcdn.com/respond/1.4.2/respond.min.js',
    ];

    /**
     * @inheritdoc
     */
    public $jsOptions = [
        'defer'     => true,
        'position'  => View::POS_HEAD,
        'condition' => 'lt IE 9',
    ];

}