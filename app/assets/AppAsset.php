<?php
/**
 * AppAsset.php file
 *
 * Date: 29.03.17
 * Time: 2:25
 * @filename AppAsset.php
 * @author   Max Goryavin <monamonax@mail.ru>
 */

namespace app\assets;


use yii\web\AssetBundle;

/**
 * Class AppAsset
 * @package  app\assets
 * @author   Max Goryavin <monamonax@mail.ru>
 */
class AppAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@webroot/public';

    /**
     * @inheritdoc
     */
    public $baseUrl = '@web';

    /**
     * @inheritdoc
     */
    public $js = [
        '//cdn.jsdelivr.net/jquery.suggestions/17.2/js/jquery.suggestions.min.js',
        'main.js',
    ];

    /**
     * @inheritdoc
     */
    public $css = [
        '//cdn.jsdelivr.net/jquery.suggestions/17.2/css/suggestions.css',
    ];


    /**
     * @var array
     */
    public $depends = [
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'rmrevin\yii\fontawesome\AssetBundle',
        'app\assets\LTIE9Asset',
    ];

    /**
     * @var array
     */
    public $jsOptions = [
        'defer' => true,
    ];

    /**
     * @inheritDoc
     */
    public function init()
    {
        parent::init();
    }
}