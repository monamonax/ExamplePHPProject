<?php
/**
 * ArrayHelper.php file
 *
 * Date: 26.03.17
 * Time: 13:43
 * @filename ArrayHelper.php
 * @author   Max Goryavin <monamonax@mail.ru>
 *
 */

namespace app\helpers;

/**
 * Class ArrayHelper
 * @package  app\helpers
 * @author   Max Goryavin <monamonax@mail.ru>
 *
 */
class ArrayHelper extends \yii\helpers\ArrayHelper
{
    /**
     * Рекурисвно оставляте в массиве только уникальные значения
     *
     * @param array $array
     *
     * @return array
     */
    public static function uniqueRecursive($array = [])
    {
        $result = [];
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $value = self::uniqueRecursive($value);
            }
            if (is_string($key)) {
                if (isset($result[$key])) {
//                $result[$key]
//                TODO : что то нужно решить
                } else {
                    $result[$key] = $value;
                }
            } elseif (is_int($key)) {
                if (array_search($value, $result) === false) {
                    $result[] = $value;
                }
            }
        }
        return $result;
    }
}