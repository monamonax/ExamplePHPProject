<?php
/**
 * main.php file
 *
 * Date: 29.03.17
 * Time: 2:20
 * @filename main.php
 * @author   Max Goryavin <monamonax@mail.ru>
 */

use yii\bootstrap\Html;

/**
 * @var \yii\web\View $this
 * @var string        $content
 */
$this->beginPage();
?>
<!doctype html>
<html lang="<?= \Yii::$app->language; ?>">
<head>
    <base href="<?= \Yii::$app->urlManager->baseUrl; ?>/">
    <?= Html::csrfMetaTags(); ?>
    <title><?= Html::encode($this->title); ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->head(); ?>
</head>

<body>
<?php $this->beginBody(); ?>
<div id="container">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">33 Slona</a>
            </div>
        </div>
    </nav>
    <div class="container">
        <?= $content; ?>
    </div>
</div>

<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>
