<?php
/**
 * index.php file
 *
 * Date: 29.03.17
 * Time: 2:19
 * @filename index.php
 * @author   Max Goryavin <monamonax@mail.ru>
 */


use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/**
 * @var \yii\web\View                $this
 * @var \app\models\Ad               $searchModel
 * @var \yii\data\ActiveDataProvider $dataProvider
 */
?>
<div class="ad-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Ad'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => false,
        'columns'      => [
            'id',
            'address:ntext',
            'name',
            'phone',
            'status' => [
                'filter'    => false,
                'attribute' => 'status',
                'value'     => function ($model, $widget) {
                    /** @var \app\models\Ad $model */
                    $statuses = \Yii::$app->moderator->getStatuses();
                    return array_key_exists($model->status, $statuses) ? $statuses[$model->status] : 'Что то тут не чисто';
                },
            ],
            [
                'class'          => \yii\grid\ActionColumn::className(),
                'contentOptions' => [
                    'class' => 'action-column',
                ],
                'buttons'        => [
                    'view' => function ($url, $model, $key) {
                        return Html::a(\rmrevin\yii\fontawesome\FA::i('eye'), $url, [
                            'class'      => 'btn btn-secondary btn-success btn-sm',
                            'title'      => Yii::t('yii', 'View'),
                            'aria-label' => Yii::t('yii', 'View'),
                            'data-pjax'  => '0',
                        ]);
                    },
                ],
                'template'       => '<div class="btn-group"> {view} </div>',
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>