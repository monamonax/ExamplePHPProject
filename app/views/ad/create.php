<?php
/**
 * create.php file
 *
 * Date: 29.03.17
 * Time: 2:30
 * @filename create.php
 * @author   Max Goryavin <monamonax@mail.ru>
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var \yii\web\View  $this
 * @var \app\models\Ad $model
 */
?>
<div class="ad-create">

    <h1><?= Html::encode($this->title) ?></h1>


    <div class="ad-form">

        <?php $form = ActiveForm::begin(); ?>

        <div class="row">
            <div class="col-sm-12">
                <?= $form->field($model, 'address')->textInput(['class' => 'form-control dadata']) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-8">
                <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
                    'mask' => '89999999999',
                ]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <?= $form->field($model, 'type')->dropDownList($model->getTypes()) ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'owner')->dropDownList($model->getOwners()) ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'price')->textInput(['type' => 'number', 'min' => 0]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-3">
                <?= $form->field($model, 'storeys')->textInput(['type' => 'number', 'min' => 0]) ?>
            </div>
            <div class="col-sm-3">
                <?= $form->field($model, 'floor')->textInput(['type' => 'number', 'min' => 0]) ?>
            </div>
            <div class="col-sm-3">
                <?= $form->field($model, 'rooms')->textInput(['type' => 'number', 'min' => 0]) ?>
            </div>
            <div class="col-sm-3">
                <?= $form->field($model, 'area')->textInput(['type' => 'number', 'min' => 0]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <?= $form->field($model, 'source')->dropDownList(Yii::$app->moderator->getSourceNames()) ?>
            </div>
        </div>


        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>