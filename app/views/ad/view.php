<?php
/**
 * view.php file
 *
 * Date: 29.03.17
 * Time: 2:32
 * @filename view.php
 * @author   Max Goryavin <monamonax@mail.ru>
 */

use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var \yii\web\View  $this
 * @var \app\models\Ad $model
 */
?>
<div class="ad-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (\app\components\Moderator::STATUS_AUTO_MODERATION == $model->status) : ?>
        <div class="alert alert-success" role="alert">Объявление создано, статус объявление автоматически обновится.
            Обновите страницу!
        </div>
    <?php endif; ?>

    <?= DetailView::widget([
        'model'      => $model,
        'attributes' => [
            'id',
            'status' => [
                'attribute' => 'status',
                'value'     => function ($model, $widget) {
                    /** @var \app\models\Ad $model */
                    $statuses = \Yii::$app->moderator->getStatuses();
                    return array_key_exists($model->status, $statuses) ? $statuses[$model->status] : 'Что то тут не чисто';
                },
            ],
            'address:ntext',
            'name',
            'phone',
            'type'   => [
                'attribute' => 'type',
                'value'     => function ($model, $widget) {
                    /** @var \app\models\Ad $model */
                    $types = $model->getTypes();
                    return array_key_exists($model->type, $types) ? $types[$model->type] : 'Что то тут не чисто';
                },
            ],
            'owner'  => [
                'attribute' => 'owner',
                'value'     => function ($model, $widget) {
                    /** @var \app\models\Ad $model */
                    $owners = $model->getOwners();
                    return array_key_exists($model->owner, $owners) ? $owners[$model->owner] : 'Что то тут не чисто';
                },
            ],
            'price',
            'storeys',
            'floor',
            'rooms',
            'area',
            'description:ntext',
            'source' => [
                'attribute' => 'source',
                'value'     => function ($model, $widget) {
                    /** @var \app\models\Ad $model */
                    $names = Yii::$app->moderator->getSourceNames();
                    return array_key_exists($model->source, $names) ? $names[$model->source] : 'Что то тут не чисто';
                },
            ],
        ],
    ]) ?>

    <hr>
    <h2>История статусов</h2>

    <?php
    foreach ($model->history as $history) {
        echo DetailView::widget([
            'model'      => $history,
            'attributes' => [
                'old_status' => [
                    'attribute' => 'old_status',
                    'value'     => function ($model, $widget) {
                        /** @var \app\models\AdHistory $model */
                        $statuses = \Yii::$app->moderator->getStatuses();
                        return array_key_exists($model->old_status, $statuses) ? $statuses[$model->old_status] : 'Что то тут не чисто';
                    },
                ],
                'new_status' => [
                    'attribute' => 'old_status',
                    'value'     => function ($model, $widget) {
                        /** @var \app\models\AdHistory $model */
                        $statuses = \Yii::$app->moderator->getStatuses();
                        return array_key_exists($model->new_status, $statuses) ? $statuses[$model->new_status] : 'Что то тут не чисто';
                    },
                ],
                'time:datetime',
                'error:ntext',
                'error_validator:ntext',
            ],
        ]);
    }
    ?>

</div>
