<?php
/**
 * SearchTrait.php file
 *
 * Date: 29.03.17
 * Time: 2:16
 * @filename SearchTrait.php
 * @author   Max Goryavin <monamonax@mail.ru>
 */

namespace app\traits;

use app\helpers\ArrayHelper;
use rmrevin\yii\fontawesome\FA;
use Yii;
use app\components\ActiveRecord;
use yii\bootstrap\Html;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\grid\ActionColumn;
use yii\grid\SerialColumn;

/**
 * Trait SearchTrait
 * @package app\traits
 */
trait SearchTrait
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        /** @var ActiveRecord $this */
        $rules   = parent::rules();
        $rules[] = [$this->attributes(), 'safe', 'on' => 'search'];
        return $rules;
    }

    /**
     * @return array
     */
    protected function getTypesColumnsByModel()
    {
        return ArrayHelper::map(\Yii::$app->db->schema->getTableSchema(call_user_func([get_class($this), 'tableName']))->columns, 'name', 'phpType');
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        /** @var ActiveRecord $this */
        $this->scenario = 'search';
        /** @var ActiveQuery $query */
        $query        = $this->find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'  => [
                'defaultOrder' => [
                    $this->primaryKey()[0] => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        return $dataProvider;
    }

}