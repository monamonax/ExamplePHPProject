<?php
/**
 * ActiveRecord.php file
 *
 * Date: 29.03.17
 * Time: 1:36
 * @filename ActiveRecord.php
 * @author   Max Goryavin <monamonax@mail.ru>
 *
 */

namespace app\components;


use app\traits\SearchTrait;

/**
 * Class ActiveRecord
 * @package  app\components
 * @author   Max Goryavin <monamonax@mail.ru>
 *
 * @property integer $id
 */
class ActiveRecord extends \yii\db\ActiveRecord
{
    use SearchTrait;

    /**
     * @inheritDoc
     */
    public function rules()
    {
        $rules = parent::rules();

        $rules['search'] = [$this->attributes(), 'safe', 'on' => 'search'];
        return $rules;
    }

    /**
     * @inheritDoc
     */
    public function attributeLabels()
    {
        $attributeLabels = parent::attributeLabels();
        foreach ($this->attributes() as $attribute) {
            $attributeLabels[$attribute] = \Yii::t('app', $attribute);
        }
        return $attributeLabels;
    }

}