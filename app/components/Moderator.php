<?php
/**
 * Moderator.php file
 *
 * Date: 29.03.17
 * Time: 1:44
 * @filename Moderator.php
 * @author   Max Goryavin <monamonax@mail.ru>
 */

namespace app\components;


use app\models\Ad;
use yii\base\Component;

/**
 * Class Moderator
 * @package  app\components
 * @author   Max Goryavin <monamonax@mail.ru>
 */
class Moderator extends Component
{

    const STATUS_AUTO_MODERATION         = 0;   // Статус "автомодерация"
    const STATUS_AUTO_MODERATION_SUCCESS = 7;   // Статус "прошло автомодерацию"
    const STATUS_AUTO_MODERATION_FAILED  = 9;   // Статус "не прошло автомодерацию"

    /**
     * @return array Все возможные статусы
     */
    public function getStatuses()
    {
        return [
            self::STATUS_AUTO_MODERATION         => 'автомодерация',
            self::STATUS_AUTO_MODERATION_SUCCESS => 'прошло автомодерацию',
            self::STATUS_AUTO_MODERATION_FAILED  => 'не прошло автомодерацию',
        ];
    }

    /**
     * @var array Массив "Источники"
     */
    private $_sources = [];

    /**
     * @var array Массив наименований "Источники"
     */
    private $_sourceNames = [];

    /**
     * @return array
     */
    public function getSources(): array
    {
        return $this->_sources;
    }

    /**
     * @param string $name
     *
     * @return ModeratorSourceInterface|null
     */
    public function getSourceByName(string $name)
    {
        if (array_key_exists($name, $this->getSources())) {
            return $this->getSources()[$name];
        }
        return null;
    }

    /**
     * @param array $sources
     */
    public function setSources(array $sources)
    {
        $this->_sources     = [];
        $this->_sourceNames = [];
        foreach ($sources as $name => $source) {
            $source = \Yii::createObject($source);
            if ($source instanceof ModeratorSourceInterface) {
                $this->_sources[$name]     = $source;
                $this->_sourceNames[$name] = $source->getName();
            }
        }
    }

    /**
     * @return array
     */
    public function getSourceNames(): array
    {
        return $this->_sourceNames;
    }

}