<?php
/**
 * ValidatorOne.php file
 *
 * Date: 29.03.17
 * Time: 4:29
 * @filename ValidatorOne.php
 * @author   Max Goryavin <monamonax@mail.ru>
 */

namespace app\components\validators;


use app\components\ModeratorValidatorInterface;
use app\models\Ad;
use yii\base\Object;

/**
 * Class ValidatorOne
 * @package  app\components\validators
 * @author   Max Goryavin <monamonax@mail.ru>
 */
class ValidatorOne extends Object implements ModeratorValidatorInterface
{
    /**
     * @var string Адрес файла с черным списком телефонов
     */
    public $fileName = '@config/phones.txt';

    /**
     * @inheritDoc
     */
    public function validate(Ad $model)
    {
        if ($this->fileName && file_exists(\Yii::getAlias($this->fileName)) &&
            false !== strpos(file_get_contents(\Yii::getAlias($this->fileName)), $model->phone)
        ) {
            return false;
        }
        return true;
    }

    /**
     * @inheritDoc
     */
    public function getErrorName()
    {
        return 'Номер телефона собственника находится в “черном списке”';
    }


}