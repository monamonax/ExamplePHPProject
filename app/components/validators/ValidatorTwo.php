<?php
/**
 * ValidatorTwo.php file
 *
 * Date: 29.03.17
 * Time: 4:30
 * @filename ValidatorTwo.php
 * @author   Max Goryavin <monamonax@mail.ru>
 */

namespace app\components\validators;


use app\components\ModeratorValidatorInterface;
use app\models\Ad;
use yii\base\Object;

/**
 * Class ValidatorTwo
 * @package  app\components\validators
 * @author   Max Goryavin <monamonax@mail.ru>
 */
class ValidatorTwo extends Object implements ModeratorValidatorInterface
{

    /**
     * @var string Адрес файла со списком стоп слов
     */
    public $fileName = '@config/stop.txt';

    /**
     * @inheritDoc
     */
    public function validate(Ad $model)
    {
        if ($this->fileName && file_exists(\Yii::getAlias($this->fileName))) {
            $stopList = explode("\n", file_get_contents(\Yii::getAlias($this->fileName)));
            foreach ($stopList as $word) {
                if (preg_match('/(' . trim($word) . ')+/ui', $model->description)) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * @inheritDoc
     */
    public function getErrorName()
    {
        return 'Описание содержит “стоп слова”';
    }


}