<?php
/**
 * ValidatorFive.php file
 *
 * Date: 29.03.17
 * Time: 4:31
 * @filename ValidatorFive.php
 * @author   Max Goryavin <monamonax@mail.ru>
 */

namespace app\components\validators;


use app\components\ModeratorValidatorInterface;
use app\models\Ad;
use yii\base\Object;

/**
 * Class ValidatorFive
 * @package  app\components\validators
 * @author   Max Goryavin <monamonax@mail.ru>
 */
class ValidatorFive extends Object implements ModeratorValidatorInterface
{
    /**
     * @inheritDoc
     */
    public function validate(Ad $model)
    {
        return 0 == $model->hasDuplicate();
    }

    /**
     * @inheritDoc
     */
    public function getErrorName()
    {
        return 'Найден дубликат';
    }


}