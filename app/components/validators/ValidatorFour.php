<?php
/**
 * ValidatorFour.php file
 *
 * Date: 29.03.17
 * Time: 4:31
 * @filename ValidatorFour.php
 * @author   Max Goryavin <monamonax@mail.ru>
 */

namespace app\components\validators;

use app\components\ModeratorValidatorInterface;
use app\models\Ad;
use yii\base\Object;

/**
 * Class ValidatorFour
 * @package  app\components\validators
 * @author   Max Goryavin <monamonax@mail.ru>
 */
class ValidatorFour extends Object implements ModeratorValidatorInterface
{
    /**
     * @inheritDoc
     */
    public function validate(Ad $model)
    {
        $search = Ad::find();
        if (Ad::OWNER_ONE == $model->owner && 5 < $search->where(['phone' => $model->phone])->count()) {
            return false;
        }
        return true;
    }

    /**
     * @inheritDoc
     */
    public function getErrorName()
    {
        return 'Подозрительное объявление';
    }


}