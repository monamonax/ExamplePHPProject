<?php
/**
 * ValidatorThree.php file
 *
 * Date: 29.03.17
 * Time: 4:31
 * @filename ValidatorThree.php
 * @author   Max Goryavin <monamonax@mail.ru>
 */

namespace app\components\validators;

use app\components\ModeratorValidatorInterface;
use app\models\Ad;
use yii\base\Object;

/**
 * Class ValidatorThree
 * @package  app\components\validators
 * @author   Max Goryavin <monamonax@mail.ru>
 */
class ValidatorThree extends Object implements ModeratorValidatorInterface
{
    /**
     * @inheritDoc
     */
    public function validate(Ad $model)
    {
        if (preg_match('/(продажа от собственника)+/ui', $model->description) && Ad::OWNER_TWO == $model->owner) {
            return false;
        }
        return true;
    }

    /**
     * @inheritDoc
     */
    public function getErrorName()
    {
        return 'Несоответствие описания';
    }


}