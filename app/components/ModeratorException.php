<?php
/**
 * ModeratorException.php file
 *
 * Date: 29.03.17
 * Time: 4:19
 * @filename ModeratorException.php
 * @author   Max Goryavin <monamonax@mail.ru>
 */

namespace app\components;


use yii\base\Exception;

/**
 * Class ModeratorException
 * @package  app\components
 * @author   Max Goryavin <monamonax@mail.ru>
 */
class ModeratorException extends Exception
{

    /**
     * @var ModeratorValidatorInterface
     */
    public $validator = null;

    /**
     * @inheritDoc
     */
    public function getName()
    {
        return 'Validate Failed';
    }

    /**
     * @inheritDoc
     */
    public function __construct($validator, $message = "", $code = 0, \Exception $previous = null)
    {
        $this->validator = $validator;
        \Exception::__construct($message, $code, $previous);
    }


}