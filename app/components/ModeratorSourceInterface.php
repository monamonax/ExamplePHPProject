<?php
/**
 * ModeratorSourceInterface.php file
 *
 * Date: 29.03.17
 * Time: 3:28
 * @filename ModeratorSourceInterface.php
 * @author   Max Goryavin <monamonax@mail.ru>
 */

namespace app\components;

use app\models\Ad;

/**
 * Interface ModeratorSourceInterface
 * @package app\components
 */
interface ModeratorSourceInterface
{

    /**
     * @param Ad $model
     *
     * @return boolean
     */
    public function validate(Ad $model);

    /**
     * @return string
     */
    public function getName();
}