<?php
/**
 * SourceTwo.php file
 *
 * Date: 29.03.17
 * Time: 5:14
 * @filename SourceTwo.php
 * @author   Max Goryavin <monamonax@mail.ru>
 */

namespace app\components\sources;

use app\components\BaseModeratorSource;
use app\components\ModeratorSourceInterface;
use app\components\validators\ValidatorFive;
use app\components\validators\ValidatorFour;
use app\components\validators\ValidatorOne;
use app\components\validators\ValidatorThree;
use app\components\validators\ValidatorTwo;
use app\models\Ad;
use yii\base\Object;


class SourceTwo extends BaseModeratorSource
{
    /**
     * @inheritDoc
     */
    protected $_validators = [
        'one'   => ValidatorOne::class,
        'three' => ValidatorThree::class,
        'four'  => ValidatorFour::class,
        'five'  => ValidatorFive::class,
    ];


    /**
     * @inheritDoc
     */
    public function getName()
    {
        return 'Источник Два';
    }
}