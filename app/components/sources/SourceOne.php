<?php
/**
 * SourceOne.php file
 *
 * Date: 29.03.17
 * Time: 3:32
 * @filename SourceOne.php
 * @author   Max Goryavin <monamonax@mail.ru>
 */

namespace app\components\sources;


use app\components\BaseModeratorSource;
use app\components\ModeratorSourceInterface;
use app\components\validators\ValidatorFive;
use app\components\validators\ValidatorFour;
use app\components\validators\ValidatorThree;
use app\components\validators\ValidatorTwo;
use app\models\Ad;
use yii\base\Object;

/**
 * Class SourceOne
 * @package  app\components\sources
 * @author   Max Goryavin <monamonax@mail.ru>
 */
class SourceOne extends BaseModeratorSource
{
    /**
     * @inheritDoc
     */
    protected $_validators = [
        'two'   => ValidatorTwo::class,
        'three' => ValidatorThree::class,
        'four'  => ValidatorFour::class,
        'five'  => ValidatorFive::class,
    ];


    /**
     * @inheritDoc
     */
    public function getName()
    {
        return 'Источник Один';
    }
}