<?php
/**
 * ModeratorValidatorInterface.php file
 *
 * Date: 29.03.17
 * Time: 3:26
 * @filename ModeratorValidatorInterface.php
 * @author   Max Goryavin <monamonax@mail.ru>
 */

namespace app\components;

use app\models\Ad;

/**
 * Interface ModeratorValidatorInterface
 * @package app\components
 */
interface ModeratorValidatorInterface
{

    /**
     * @param Ad $model
     *
     * @return boolean
     */
    public function validate(Ad $model);

    /**
     * @return string
     */
    public function getErrorName();

}