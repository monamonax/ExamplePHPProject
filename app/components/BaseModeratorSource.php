<?php
/**
 * BaseModeratorSource.php file
 *
 * Date: 29.03.17
 * Time: 4:42
 * @filename BaseModeratorSource.php
 * @author   Max Goryavin <monamonax@mail.ru>
 */

namespace app\components;

use app\models\Ad;
use yii\base\Object;

/**
 * Class BaseModeratorSource
 * @package  app\components
 * @author   Max Goryavin <monamonax@mail.ru>
 */
abstract class BaseModeratorSource extends Object implements ModeratorSourceInterface
{

    /**
     * Собственно через этот массив мы можем задавать каике "валидаторы" и в какой последовательности будут
     * использованны для автомотической модерации
     * @var array
     */
    protected $_validators = [];

    /**
     * @inheritDoc
     */
    public function validate(Ad $model)
    {
        foreach ($this->_validators as $validator) {
            if (is_string($validator)) {
                $validator = \Yii::createObject($validator);
            }
            if ($validator instanceof ModeratorValidatorInterface) {
                /** @var ModeratorValidatorInterface $validator */
                if (!$validator->validate($model)) {
                    throw new ModeratorException($validator);
                }
            }
        }
        return true;
    }

}