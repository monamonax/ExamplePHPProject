<?php
/**
 * Application.php file
 *
 * Date: 29.03.17
 * Time: 2:10
 * @filename Application.php
 * @author   Max Goryavin <monamonax@mail.ru>
 */

namespace app\bootstrap;


use app\components\Moderator;
use yii\base\BootstrapInterface;

/**
 * Class Application
 * @package  app\bootstrap
 * @author   Max Goryavin <monamonax@mail.ru>
 */
class Application implements BootstrapInterface
{
    /**
     * @inheritDoc
     */
    public function bootstrap($app)
    {
//        Так как я использую https://asset-packagist.org нужно перекинуть алиасы
        \Yii::setAlias('@bower', '@vendor/bower-asset');
        \Yii::setAlias('@npm', '@vendor/npm-asset');
    }


}