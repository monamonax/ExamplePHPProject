<?php
/**
 * Application.php file
 *
 * Date: 26.03.17
 * Time: 13:23
 * @filename Application.php
 * @author   Max Goryavin <monamonax@mail.ru>
 *
 */

namespace app;

use app\helpers\ArrayHelper;
use yii\base\Event;
use yii\helpers\FileHelper;

/**
 * Class Application
 * @package  app
 * @author   Max Goryavin <monamonax@mail.ru>
 *
 */
class Application
{
    /**
     * Console application class
     */
    const CONSOLE = 'yii\\console\\Application';

    /**
     * Web application class
     */
    const WEB = 'yii\\web\\Application';

    /**
     * @var string
     */
    private $_configDir = null;

    /**
     * @var array
     */
    private $_config = [];

    /**
     * Application constructor.
     *
     * @param $configDir
     */
    public function __construct(string $configDir)
    {
        $this->_configDir = \Yii::getAlias($configDir);
    }

    /**
     * Запуск приложения
     */
    public function run()
    {
        exit(self::instance(APP_CLI ? self::CONSOLE : self::WEB, $this->getConfig())->run());
    }

    /**
     * @return string
     */
    public function getConfigDir(): string
    {
        return $this->_configDir;
    }

    /**
     * Создаем конфиг из файлов
     * @return array
     */
    private function getConfig()
    {
        $configNames = [
            'app',
        ];
        if (APP_CLI) {
            $configNames[] = 'console';
        } else {
            $configNames[] = 'web';
        }
        $configNames[] = YII_ENV;

        if (empty($this->_config)) {
            $config = [];
            foreach ($configNames as $name) {
                $config = ArrayHelper::uniqueRecursive(ArrayHelper::merge($config, $this->getConfigFile($name)));
            }
            $this->_config = $config;
        }
        return $this->_config;
    }

    /**
     * Загрузим сонфиг из файла
     *
     * @param $configName
     *
     * @return array|mixed
     */
    private function getConfigFile($configName)
    {
        $config = [];
        if (is_file($this->_configDir . DS . $configName . '.php')) {
            try {
                $config = require $this->_configDir . DS . $configName . '.php';
            } catch (\Exception $exception) {
//                TODO : Завернуть все через лог
            }
        }
        return $config;
    }

    /**
     * @param string $type
     * @param array  $config
     *
     * @return \yii\web\Application|\yii\console\Application
     */
    public static function instance($type = self::WEB, $config = [])
    {
        return new $type($config);
    }


}

/**
 * add Yii to global namespace
 */
require_once 'Yii.php';
