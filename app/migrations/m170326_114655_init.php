<?php

use yii\db\Migration;

/**
 * Class m170326_114655_init
 * @author   Max Goryavin <monamonax@mail.ru>
 *
 */
class m170326_114655_init extends Migration
{
    /**
     * @inheritDoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        /**
         * Таблица Объявлений
         */
        $this->createTable('{{%ad}}', [
            'id'          => $this->primaryKey(),
            'address'     => $this->text(),
            'name'        => $this->string(255),
            'phone'       => $this->string(255),
            'status'      => $this->smallInteger(2)->unsigned()->defaultValue(0),
            'type'        => $this->smallInteger(2),
            'price'       => $this->money(10, 2),
            'owner'       => $this->smallInteger(2)->unsigned()->defaultValue(0),
            'floor'       => $this->integer(4),
            'storeys'     => $this->integer(4)->unsigned(),
            'rooms'       => $this->integer(4),
            'area'        => $this->string(100),
            'description' => $this->text(),
            'source'      => $this->string(255)->defaultValue(''),
        ], $tableOptions);

        /**
         * Таблица истории
         */
        $this->createTable('{{%ad_history}}', [
            'id'              => $this->primaryKey(),
            'ad_id'           => $this->integer(11)->unsigned(),
            'old_status'      => $this->smallInteger(2)->unsigned()->defaultValue(0),
            'new_status'      => $this->smallInteger(2)->unsigned()->defaultValue(0),
            'error'           => $this->string(255)->defaultValue(''),
            'error_validator' => $this->string(255)->defaultValue(''),
            'time'            => $this->integer(11)->unsigned(),
        ], $tableOptions);
        return true;
    }

    /**
     * @inheritDoc
     */
    public function down()
    {
        $this->dropTable('{{%ad}}');
        $this->dropTable('{{%ad_history}}');
        return true;
    }
}
