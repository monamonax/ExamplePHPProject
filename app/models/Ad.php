<?php
/**
 * Ad.php file
 *
 * Date: 29.03.17
 * Time: 1:35
 * @filename Ad.php
 * @author   Max Goryavin <monamonax@mail.ru>
 *
 */

namespace app\models;


use app\components\ActiveRecord;
use app\components\Moderator;
use app\components\ModeratorException;
use app\components\ModeratorSourceInterface;
use app\jobs\ModerationAd;

/**
 * Class Ad
 * @package  app\models
 * @author   Max Goryavin <monamonax@mail.ru>
 *
 * @property string      $address
 * @property string      $name
 * @property string      $phone
 * @property integer     $status
 * @property integer     $type
 * @property float       $price
 * @property string      $owner
 * @property integer     $floor
 * @property integer     $storeys
 * @property integer     $rooms
 * @property string      $area
 * @property string      $description
 * @property string      $source
 * @property AdHistory[] $history
 */
class Ad extends ActiveRecord
{

    const TYPE_SALE = 0;    // Тип продажа
    const TYPE_RENT = 5;    // Тип аренда

    const OWNER_ONE = 0;    // Собственник
    const OWNER_TWO = 8;    // Риэлтор


    /**
     * @inheritDoc
     */
    public static function tableName()
    {
        return '{{%ad}}';
    }

    /**
     * @inheritDoc
     */
    public function rules()
    {
        $rules   = parent::rules();
        $rules[] = [['address', 'name', 'phone', 'type', 'price', 'owner', 'floor', 'storeys', 'rooms', 'area', 'source'], 'required', 'on' => ['create']];
        $rules[] = [['address', 'description', 'source'], 'string'];
        $rules[] = [['name', 'phone'], 'string', 'max' => 255];
        $rules[] = [['area'], 'string', 'max' => 100];
        $rules[] = [['status'], 'in', 'range' => \Yii::$app->moderator->getStatuses()];
        $rules[] = [['status'], 'default', 'value' => Moderator::STATUS_AUTO_MODERATION];
        $rules[] = [['type'], 'in', 'range' => [self::TYPE_SALE, self::TYPE_RENT]];
        $rules[] = [['type'], 'default', 'value' => self::TYPE_SALE];
        $rules[] = [['owner'], 'in', 'range' => [self::OWNER_ONE, self::OWNER_TWO]];
        $rules[] = [['owner'], 'default', 'value' => self::OWNER_ONE];
        $rules[] = [['floor', 'storeys', 'rooms'], 'integer', 'on' => 'create'];
        $rules[] = [['price'], 'number', 'on' => 'create'];
        $rules[] = [['phone'], 'match', 'pattern' => '/^8[\d]{10}$/', 'message' => \Yii::t('app', 'Номер телефона должен начинаться с 8 и состоять только из цифр')];
        return $rules;
    }

    /**
     * @return array Типы для поля "Продажа/аренда"
     */
    public function getTypes()
    {
        return [
            self::TYPE_SALE => 'Продажа',
            self::TYPE_RENT => 'Аренда',
        ];
    }

    /**
     * @return array Для поля "Собственник/Риэлтор"
     */
    public function getOwners()
    {
        return [
            self::OWNER_ONE => 'Собственник',
            self::OWNER_TWO => 'Риэлтор',
        ];
    }

    public function automaticModeration()
    {
        /** @var ModeratorSourceInterface $source */
        $source = \Yii::$app->moderator->getSourceByName($this->source);
        if (null !== $source) {
            try {
                if (true === $source->validate($this)) {
                    $this->changeStatus(Moderator::STATUS_AUTO_MODERATION_SUCCESS);
                }
            } catch (ModeratorException $exception) {
                $this->changeStatus(Moderator::STATUS_AUTO_MODERATION_FAILED, $exception->validator->getErrorName(), get_class($exception->validator));
            }
        }
    }

    protected function changeStatus($status, $error = null, $errorValidator = null)
    {
        /** @var AdHistory $history */
        $history = \Yii::createObject(AdHistory::className());
        $history->setScenario('create');
        $history->setAttributes([
            'ad_id'           => $this->primaryKey,
            'old_status'      => $this->status,
            'new_status'      => $status,
            'error'           => $error,
            'error_validator' => $errorValidator,
        ]);
        if ($history->save()) {
            $this->updateAttributes(['status' => $status]);
        }
    }

    /**
     * @inheritDoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
//            Если модель новая, то ставим в очередь на автоматическую модерацию
            \Yii::$app->queue->push(new ModerationAd([
                'primaryKey' => $this->id,
            ]));
        }
        parent::afterSave($insert, $changedAttributes);
    }

//    ------------------------------------------------------------

    /**
     * Проверяем есть ли дубликаты по этому объявлению
     * @return bool
     */
    public function hasDuplicate()
    {
        $search = Ad::find();
        $result = $search->where([
            'address' => $this->address,
            'price'   => $this->price,
            'floor'   => $this->floor,
            'rooms'   => $this->rooms,
            'area'    => $this->area,
        ])->andWhere('id <> :ID', [':ID' => $this->id])->count();
        return $result > 0;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHistory()
    {
        return $this->hasMany(AdHistory::className(), ['ad_id' => 'id'])->orderBy(['time' => SORT_DESC]);
    }


}