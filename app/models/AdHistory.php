<?php
/**
 * AdHistory.php file
 *
 * Date: 29.03.17
 * Time: 4:15
 * @filename AdHistory.php
 * @author   Max Goryavin <monamonax@mail.ru>
 */

namespace app\models;


use app\components\ActiveRecord;

/**
 * Class AdHistory
 * @package  app\models
 * @author   Max Goryavin <monamonax@mail.ru>
 *
 * @property integer        $ad_id
 * @property integer        $old_status
 * @property integer        $new_status
 * @property string         $error
 * @property string         $error_validator
 * @property integer|string $time
 */
class AdHistory extends ActiveRecord
{

    /**
     * @inheritDoc
     */
    public static function tableName()
    {
        return '{{%ad_history}}';
    }

    /**
     * @inheritDoc
     */
    public function rules()
    {
        $rules   = parent::rules();
        $rules[] = [['ad_id', 'old_status', 'new_status',], 'required', 'on' => ['create']];
        $rules[] = [['error', 'error_validator'], 'string', 'max' => 255];
        $rules[] = [['time'], 'default', 'value' => time()];
        $rules[] = [['old_status', 'new_status', 'ad_id'], 'integer', 'on' => 'create'];
        return $rules;
    }
}