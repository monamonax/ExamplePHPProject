<?php
/**
 * bootstrap.php file
 *
 * Date: 26.03.17
 * Time: 13:19
 * @filename bootstrap.php
 * @author   Max Goryavin <monamonax@mail.ru>
 *
 */

$composerLoader = require_once __DIR__ . "/vendor/autoload.php";

define('YII_DEBUG', true);
define('YII_ENV', 'dev');
define('APP_LOCALE', 'en_US.utf8');
define('APP_CLI', php_sapi_name() == 'cli');
define('DS', DIRECTORY_SEPARATOR);

setlocale(LC_ALL, APP_LOCALE);
date_default_timezone_set('UTC');

if (YII_DEBUG) {
    error_reporting(E_ALL);
} else {
    error_reporting(0);
}

$app = new \app\Application(__DIR__ . '/config');
$app->run();