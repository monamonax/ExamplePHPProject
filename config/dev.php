<?php
/**
 * dev.php file
 *
 * Date: 26.03.17
 * Time: 13:21
 * @filename dev.php
 * @author   Max Goryavin <monamonax@mail.ru>
 *
 */
return [
    'bootstrap' => [
        'gii',
        'debug',
    ],
    'components' => [
        'db'     => [
            'dsn'                 => 'mysql:host=localhost;dbname=dev_33slona',
            'username'            => 'root',
            'password'            => 'qweqwe123123',
            'enableSchemaCache'   => false,
            'schemaCacheDuration' => 3600,
            'schemaCache'         => 'cache',
        ],
        'queue'  => [
            'driver' => [
                'class'  => \zhuravljov\yii\queue\sync\Driver::class,
                'handle' => true, // Флаг необходимости выполнять поставленные в очередь задания
            ],
        ],
    ],
    'modules' => [
        'gii' => [
            'class' => 'yii\gii\Module',
        ],
        'debug' => [
            'class'     => \yii\debug\Module::className(),
            'traceLine' => '<a href="phpstorm://open?url={file}&line={line}">{file}:{line}</a>',
            'panels'    => [
                'queue' => \zhuravljov\yii\queue\debug\Panel::class,
            ],
        ],
    ],
];