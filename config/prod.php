<?php
/**
 * prod.php file
 *
 * Date: 26.03.17
 * Time: 13:21
 * @filename prod.php
 * @author   Max Goryavin <monamonax@mail.ru>
 *
 */
return [
    'components' => [
        'db'     => [
            'dsn'                 => 'mysql:host=localhost;dbname=',
            'username'            => '',
            'password'            => '',
            'enableSchemaCache'   => false,
            'schemaCacheDuration' => 3600,
            'schemaCache'         => 'cache',
        ],
        'queue'  => [
            'driver' => [
                'class'  => \zhuravljov\yii\queue\sync\Driver::class,
                'handle' => false, // Флаг необходимости выполнять поставленные в очередь задания
            ],
        ],
    ],
];