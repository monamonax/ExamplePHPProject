<?php
/**
 * console.php file
 *
 * Date: 26.03.17
 * Time: 13:21
 * @filename console.php
 * @author   Max Goryavin <monamonax@mail.ru>
 *
 */
return [
    'controllerNamespace' => 'console\\controllers',
    'controllerMap'       => [
    ],
    'components'          => [
        'request' => [
            'class' => \yii\console\Request::className(),
        ],
    ],
];