<?php
/**
 * web.php file
 *
 * Date: 26.03.17
 * Time: 13:21
 * @filename web.php
 * @author   Max Goryavin <monamonax@mail.ru>
 *
 */
return [
    'controllerNamespace' => 'app\\controllers',
    'defaultRoute'        => 'ad/index',
    'components'          => [
        'view'         => [
        ],
        'urlManager'   => [
            'enablePrettyUrl' => true,
            'showScriptName'  => false,
            'rules'           => [
            ],
        ],
        'request'      => [
            'enableCsrfValidation' => true,
            'cookieValidationKey'  => '33slona',
            'parsers'              => [
            ],
        ],
        'errorHandler' => [
            'errorAction' => '/home/error',
        ],
    ],
];