<?php
/**
 * app.php file
 *
 * Date: 26.03.17
 * Time: 13:20
 * @filename app.php
 * @author   Max Goryavin <monamonax@mail.ru>
 *
 */
return [
    'id'             => '33solna',
    'name'           => '33solna',
    'version'        => '1.0',
    'language'       => 'ru-RU',
    'sourceLanguage' => 'ru-RU',
    'timeZone'       => 'Europe/Moscow',
    'basePath'       => \yii\helpers\FileHelper::normalizePath(dirname(__DIR__) . DS . 'app'),
    'bootstrap'      => [
        \app\bootstrap\Application::class,
    ],
    'aliases'        => [
        '@console' => \yii\helpers\FileHelper::normalizePath(dirname(__DIR__) . DS . 'console'),
        '@modules' => \yii\helpers\FileHelper::normalizePath(dirname(__DIR__) . DS . 'modules'),
        '@config'  => \yii\helpers\FileHelper::normalizePath(dirname(__DIR__) . DS . 'config'),
    ],
    'vendorPath'     => '@app/../vendor',
    'runtimePath'    => '@app/../runtime',
    'modules'        => [
    ],
    'components'     => [
        'moderator' => [
            'class' => \app\components\Moderator::className(),
            'sources' => [
                'sourceOne' => \app\components\sources\SourceOne::class,
                'sourceTwo' => \app\components\sources\SourceTwo::class,
            ],
        ],
        'formatter'    => [
            'locale'            => 'ru-RU',
            'dateFormat'        => 'php:d.m.Y',
            'datetimeFormat'    => 'php:d.m.Y H:i',
            'defaultTimeZone'   => 'UTC',
            'decimalSeparator'  => ',',
            'thousandSeparator' => ' ',
            'currencyCode'      => 'RUR',
        ],
        'view'         => [
        ],
        'assetManager' => [
            'linkAssets' => true,
        ],
        'db'           => [
            'class'       => yii\db\Connection::className(),
            'dsn'         => 'mysql:host=localhost;dbname=',
            'username'    => '',
            'password'    => '',
            'charset'     => 'utf8',
            'tablePrefix' => 'tbl_',
        ],
        'log'          => [
        ],
        'i18n'         => [
            'translations' => [
                'app' => [
                    'class'          => yii\i18n\PhpMessageSource::className(),
                    'basePath'       => '@app/messages',
                    'sourceLanguage' => 'en-US',
                    'fileMap'        => [
                        'app' => 'app.php',
                    ],
                ],
            ],
        ],
        'cache'        => [
            'class' => yii\caching\FileCache::className(),
        ],
        'queue'  => [
            'class'  => \zhuravljov\yii\queue\Queue::className(),
            'driver' => [
            ],
        ],
    ],
];